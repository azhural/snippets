/**
 * Using boost::bind in parent ctor and defining function in derived classes.
 * Usecase: creating ROS controller interfaces with action servers and defining
 * the callback in derived controllers.
 */

#include <iostream>
#include <boost/function.hpp>
#include <boost/bind.hpp>

class A{
public:
    boost::function<void (void)> f_object;
    A();
private:
    virtual void f() = 0;
};

class B : public A{
private:
    virtual void f(){
        std::cout << "B" << std::endl;
    }
};

class C : public A{
private:
    virtual void f(){
        std::cout << "C" << std::endl;
    }
};

A::A(){
    f_object = boost::bind(&A::f, this);
}

int main(){
    B b;
    b.f_object(); // B

    std::shared_ptr<A> ptr (new C);
    ptr->f_object(); // C
}
